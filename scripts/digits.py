import time
from sense_hat import SenseHat

sense = SenseHat()

X = [255, 0, 0]  # Red
O = [0, 0, 0]  # off

question_mark = [
O, O, O, X, X, O, O, O,
O, O, X, O, O, X, O, O,
O, O, O, O, O, X, O, O,
O, O, O, O, X, O, O, O,
O, O, O, X, O, O, O, O,
O, O, O, X, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, X, O, O, O, O
]

sense.set_pixels(question_mark)
time.sleep(2)
sense.clear()

#humidity = sense.get_humidity()

# alternatives
humidity  = f'{sense.humidity:.1f}'
sense.show_message('T ' + humidity, text_colour=[255,0,0])
