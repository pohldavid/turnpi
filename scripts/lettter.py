from time import sleep
from sense_hat import SenseHat

sense = SenseHat()

X = [255, 0, 0]  # Red
#O = [255, 255, 255]  # White
O = [0,0,0]  # Black

question_mark = [
X, X, X, O, O, O, O, O,
O, X, O, O, O, O, O, O,
O, X, O, O, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O
]

sense.set_pixels(question_mark)
sleep(2)
sense.clear()