#Import all neccessary features to code.
from time import sleep
import RPi.GPIO as GPIO
from sense_hat import SenseHat




#If code is stopped while the solenoid is active it stays active
#This may produce a warning if the code is restarted and it finds the GPIO Pin, which it defines as non-active in next line, is still active
#from previous time the code was run. This line prevents that warning syntax popping up which if it did would stop the code running.
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

arm_relay_pin = 17
relay_1_pin = 27
relay_2_pin = 22

GPIO.setup(arm_relay_pin, GPIO.OUT)
GPIO.setup(relay_1_pin, GPIO.OUT)
GPIO.setup(relay_2_pin, GPIO.OUT)

#Turn off all relays
GPIO.outpu(arm_relay_pin,1)
GPIO.output(relay_1_pin,1)
GPIO.output(relay_2_pin,1)


def turn_cw():
    #set relays to turn cw
    pass

def turn_ccw():
    #set relays to turn ccw
    pass

def stop():
    pass

sense = SenseHat()

# roll is  rotation about the long axis of the sense hat
# see https://projects-static.raspberrypi.org/projects/generic-theory-pitch-roll-yaw/1da6c9e518533fe8c1f70d7445fd6880d7dac12a/en/images/orientation.png
# this assumes the pi is mounted "vertically"


heading = 0

def turn_to_heading(heading, whichway):

    if whichway  == 'cw':
        #code for turning cw until heading is reached

     while True:

        o = sense.get_orientation()
        roll = o["roll"]
        rollf = f'roll: {roll:03.1f}'
        print(f'roll: {roll:03.1f}')
        print(rollf)
        turn_cw()
        if heading - 1 <= roll <= heading +1:
            stop()
            print("STOP")


    elif whichway == 'ccw':
        #code for turning ccw unitl heading is reached
        pass
    else:
        #calculate which way si shortest and go that way
        pass
