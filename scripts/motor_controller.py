#! /usr/bin/python3
import sys
from gpiozero import OutputDevice
from time import sleep


"""                      Wiring Schematic


|--------------|     R1 NO     R2 NO               |-------------|
|              |      /        /                   |             |
|          Pos |--1---o o--+---o o----3--+---------| Pos         |
|              |           |             |         |             |
|              |           2             |         |             |
|              |           | R3 NC  _    |         |             |
|     12V      |           --------o o---)-----    |    12VDC    |---|
|   Battery    |                         |    |    |    Motor    |   |
|              |             R3 NC  _    |    |    |             |---|
|              |           --------o o----    |    |             |
|              |           |                  |    |             |
|              |           |    / R2 NO       |    |             |
|          Neg |------4----+---o o---------5--+----| Neg         |
|              |                                   |             |
|--------------|                                   |-------------|

1 = #################################Orange
2 = #################################Blue/White
3 = 
5 = #################################Orange/White



leftRelay = OutputDevice(2)
rightRelay = OutputDevice(3)
stopRelay = OutputDevice(4)


def action(action):

    print("action ", action)

    if action == "stop":
        stopRelay.off()
        sleep(1)
        stopRelay.off()

    elif action == "left":
        leftRelay.off()
        sleep(1)
        leftRelay.on()

    elif action == "right":
        rightRelay.off()
        sleep(1)
        rightRelay.on()
        
    return action


if __name__ == "__main__":

    if len(sys.argv) >1 :
        
        action(sys.argv[1])
    else:
        action("stop")



