import time
import smbus
import sys

DEVICE_BUS = 1
DEVICE_ADDR = 0x10
bus = smbus.SMBus(DEVICE_BUS)

def relay_on(i):
    bus.write_byte_data(DEVICE_ADDR, i, 0xFF)
    
def relay_off(i):
    bus.write_byte_data(DEVICE_ADDR, i, 0x00)  
    
"""
while True:
    try:
        for i in range(1,5):
            relay_on(i)
            time.sleep(1)
            relay_off(i)
            time.sleep(1) 
    except KeyboardInterrupt as e:
        relay_off(i)
        print("Quit the Loop")
        sys.exit()
"""


relay_off(3)
relay_off(2)
relay_off(1)

def cw():
    
    relay_on(1)
    relay_on(2)
    relay_on(3)
    time.sleep(1)
    relay_off(3)
    relay_off(2)
    relay_off(1)
    
    


def ccw():
    relay_on(3)
    time.sleep(1)
    relay_off(3)
    
    
cw()    
time.sleep(.5)
ccw()

