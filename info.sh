

#report system info to console

echo '\r'
echo "---------- Raspberry Pi Information----------"
echo '\r'
date

echo '\r'
echo "Model" 
cat /sys/firmware/devicetree/base/model
echo '\r'

echo "OS Release"
cat /etc/os-release
echo '\r'

echo Hostname
hostname
echo '\r'

echo CPU Core Temperature
vcgencmd measure_temp

echo '\n'
echo Voltages

vcgencmd measure_volts core

for id in core sdram_c sdram_i sdram_p ; do \
echo "$id:\t$(vcgencmd measure_volts $id)" ; \
done

echo '\n'
echo Codecs

for codec in H264 MPG2 WVC1 MPG4 MJPG WMV9 ; do \
echo "$codec:\t$(vcgencmd codec_enabled $codec)" ; \
done

