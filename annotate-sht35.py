#!/usr/bin/python3

import os
import smbus
import time

'''
https://pinout.xyz/pinout/i2c#

my particular device :
https://wiki.seeedstudio.com/Grove-I2C_High_Accuracy_Temp%26Humi_Sensor-SHT35/#play-with-raspberry-pi-with-grove-base-hat-for-raspberry-pi

notes on using i2c-tools:
https://github.com/OlivierdenOuden/Sensirion_SHT35
'''

#user_annotate.txt is a ram file - create it after reboot 
os.system('echo "init" > /dev/shm/mjpeg/user_annotate.txt')


# Get I2C bus
bus = smbus.SMBus(1)

while True:

    # SHT31 address, 0x45
    bus.write_i2c_block_data(0x45, 0x2C, [0x06])
     
    time.sleep(1)

    # SHT31 address, 0x45
    # Read data back from 0x00(00), 6 bytes
    # Temp MSB, Temp LSB, Temp CRC, Humididty MSB, Humidity LSB, Humidity CRC
    data = bus.read_i2c_block_data(0x45, 0x00, 6)
     
    # Convert the data
    temp = data[0] * 256 + data[1]
    cTemp = -45 + (175 * temp / 65535.0)
    fTemp = -49 + (315 * temp / 65535.0)
    humidity = 100 * (data[3] * 256 + data[4]) / 65535.0
     
    # Output data to screen
    annotation = "\n" + " Temp: %.1fF" %fTemp + " Humidity: %.1f%%" %humidity
    print(annotation)
    
    with open('/dev/shm/mjpeg/user_annotate.txt', 'w') as outfile:
        outfile.write(annotation)

    time.sleep(20)