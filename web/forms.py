
from flask_wtf import FlaskForm
from wtforms import (StringField, IntegerField, BooleanField,
                     RadioField, SelectField, SubmitField)
from wtforms.validators import InputRequired, Length


class DirectionForm(FlaskForm):
    
    bearing = IntegerField('Bearing', default = 280)
    
    ordinate = RadioField('Direction',
                       choices=['North', 'South', 'East', 'West'],
                       validators=[InputRequired()])
    
    available = BooleanField('Available', default='checked')

    direction = SelectField('Direction', choices=[('North', 'north'), ('East', 'east'), ('South', 'south'), ('West', 'west')])

    submit = SubmitField('Submit')






      

