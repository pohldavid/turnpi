
from flask import Flask, render_template, request

from gpiozero import OutputDevice#, Button
import time
import os
import davesTime

relayOpen = OutputDevice(2, active_high=False, initial_value=False)
relayStop = OutputDevice(3, active_high=False, initial_value=False)
relayClose = OutputDevice(4, active_high=False, initial_value=False)
#btnClosed = Button

def log_action(txt):
    with open("./logs/log.txt","a+") as openfile:
        openfile.write(txt+"\n")
    return

def open_door():
#    os.system('play ./media/open-door.mp3')
    relayStop.on()
    time.sleep(0.5)
    relayStop.off()
    time.sleep(0.5)
    relayOpen.on()
    time.sleep(0.5)
    relayOpen.off()

def close_door():
#    os.system('play ./media/close-door.mp3')
    relayStop.on()
    time.sleep(0.5)
    relayStop.off()
    time.sleep(0.5)
    relayClose.on()
    time.sleep(0.5)
    relayClose.off()
    
def stop_door():
#    os.system('play ./media/stop-door.mp3')
    relayStop.on()
    time.sleep(0.5)
    relayStop.off()

app = Flask(__name__)
# Enable debug mode
app.config['DEBUG'] = True

@app.route("/")
def index():
    return render_template('index.html', action = '?')

@app.route("/video")
def video():
    return render_template('video.html')

@app.route('/log')
def log():
    loglist=[]
    with open('./logs/log.txt','r') as logfile:
        for line in logfile:
            loglist.append(line)

    return render_template('log.html',loglist=loglist)

@app.route('/home')
def home():
    return render_template('home.html')


@app.route('/cost cal')
def cost_calculator():
    return render_template('cost_calculator.html')


""" there are three submit buttons - each treated as an individual
    form.  No need to check request.form """

@app.route("/openbutton", methods=["POST"])
def openbutton():
    log_action(davesTime.get_date_and_time() + " opened")
    open_door()
    return render_template('index.html', action = 'open')

@app.route("/stopbutton", methods=["POST"])
def stopbutton():
    log_action(davesTime.get_date_and_time() + " stopped")
    stop_door()
    return render_template('index.html', action = 'stop')

@app.route("/closebutton", methods=["POST"])
def closebutton():
    log_action(davesTime.get_date_and_time() + " closed")
    close_door()
    return render_template('index.html', action = 'close')

# Run the app on the local development server
if __name__ == "__main__":
    app.run(debug = True, host = "0.0.0.0", port = 8000)
