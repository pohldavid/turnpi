#!/usr/bin/python3
from time import sleep
from sense_hat import SenseHat

sense = SenseHat()


def fetch():
    """read sensehat values and write to file"""

    while True:

        yaw = f'{sense.get_orientation()["yaw"]:03.1f}'

        humidity = f"{sense.humidity:.1f}" + "%"

        temperature = f"{((sense.temperature*9/5)+32) :.1f} F"

        pressure = f"{(sense.pressure/33.864) :.1f} InHg"

        data = f"Direction: {yaw} Humidity: {humidity} Pressure: {pressure} Temperature: {temperature}"

        print(data)

        with open("data", "w") as outfile:
            outfile.write(data)
        outfile.close

        sleep(1)

if __name__ == "__main__":
    
    fetch()