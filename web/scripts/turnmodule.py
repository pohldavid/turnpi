import time
import smbus
import sys
from sense_hat import SenseHat
sense = SenseHat()

from orient import orientation

DEVICE_BUS = 1
DEVICE_ADDR = 0x10
bus = smbus.SMBus(DEVICE_BUS)

def relay_on(i):
    bus.write_byte_data(DEVICE_ADDR, i, 0xFF)
    
def relay_off(i):
    bus.write_byte_data(DEVICE_ADDR, i, 0x00)  
    
"""
while True:
    try:
        for i in range(1,5):
            relay_on(i)
            time.sleep(1)
            relay_off(i)
            time.sleep(1) 
    except KeyboardInterrupt as e:
        relay_off(i)
        print("Quit the Loop")
        sys.exit()
"""

def direction():
    
    o = sense.get_orientation() #orientation is dictionary {'roll': 360.000, 'pitch':360.000, 'yaw':360.000}
    roll = o["roll"]
    print(f'roll: {roll:03.1f}')
    """
    if 178 < yaw < 182:
        print("STOP")
    """

#relay_off(4)
def off():
    
    relay_off(3)
    relay_off(2)
    relay_off(1)
    
off()    

def ccw(duration):
    
    direction()
    
    relay_on(1)
    relay_on(2)
    relay_on(3)
    time.sleep(duration)
    off()
    time.sleep(1)
    direction()
    
def cw(duration):
    direction()
    relay_on(3)
    time.sleep(duration)
    relay_off(3)
    time.sleep(1)
    direction()
    
def buzzer():
    relay_on(4)
    time.sleep(.1)
    relay_off(4)
    
  
    
#time.sleep(.2)
#ccw()
#buzzer()
