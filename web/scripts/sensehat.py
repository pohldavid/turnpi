from time import sleep
from sense_hat import SenseHat
sense = SenseHat()

blue = (0,0,255)
yellow = (255,255,0)

#sense.show_message("Temp = 78", text_colour=yellow, back_colour=blue)

sense.clear()

pressure = sense.get_pressure()
print(pressure)
temp = sense.get_temperature()
print(temp)
humidity = sense.get_humidity()
print(humidity)

o = sense.get_orientation()
pitch = o["pitch"]
roll = o["roll"]
yaw = o["yaw"]
print("pitch {0} roll {1} yaw {2}".format(pitch, roll, yaw))
