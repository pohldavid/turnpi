from time import sleep
from sense_hat import SenseHat
sense = SenseHat()
while True:
    
    o = sense.get_orientation() #orientation is dictionary {'roll': 360.000, 'pitch':360.000, 'yaw':360.000}
    yaw = o["yaw"]
    roll = o["roll"]
    pitch = o["pitch"]
    print(f'yaw: {yaw:03.1f} roll: {roll:03.1f} pitch: {pitch:03.1f}')
    sleep(1)    
    if 178 < yaw < 182:
        print("STOP")
    


