import time
import smbus

from sense_hat import SenseHat
sense = SenseHat()

DEVICE_BUS = 1
DEVICE_ADDR = 0x10
bus = smbus.SMBus(DEVICE_BUS)

def relay_on(i):
    bus.write_byte_data(DEVICE_ADDR, i, 0xFF)
    
def relay_off(i):
    bus.write_byte_data(DEVICE_ADDR, i, 0x00)  
    
"""
while True:
    try:
        for i in range(1,5):
            relay_on(i)
            time.sleep(1)
            relay_off(i)
            time.sleep(1) 
    except KeyboardInterrupt as e:
        relay_off(i)
        print("Quit the Loop")
        sys.exit()
"""

def on():
    relay_on(4)
    
def off():
    relay_off(4)

off()

while True:
    temperature = f'{((sense.temperature*9/5)+32) :.1f}'
    print('temperature', temperature, type(temperature))
    temp = float(temperature)
    
    if temp > 100.0:
        on()
        time.sleep(60)
        off()
        time.sleep(60)
