import os
import time
from sense_hat import SenseHat

import characters

sense = SenseHat()
sense.set_rotation(90)

while True:
    
    #add code for dimming at night
    sense.low_light = True
    #sense.set_pixels(characters.arrow)
    #sense.set_pixels(characters.all_white)
    time.sleep(1)
    sense.clear()

    #humidity = sense.get_humidity()

    humidity  = f'{sense.humidity:.1f}'
    
    temperature = f'{((sense.temperature*9/5)+32) :.1f}'

    cpu_temp = os.system("/usr/bin/vcgencmd measure_temp")

    print(cpu_temp)
    
    #######change this to in Hg
    pressure = f'{(sense.pressure*0.02953) :.2f}'
    
    sense.show_message('T ' + temperature, text_colour=[0,255,0], scroll_speed = 0.07)
    sense.show_message('H ' + humidity, text_colour=[255,0,0], scroll_speed = 0.07)
    sense.show_message('P ' + pressure, text_colour=[0,0,255], scroll_speed = 0.07)
    
    print(temperature)


    time.sleep(1)

