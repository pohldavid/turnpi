from sense_hat import SenseHat

sense = SenseHat()
north = sense.get_compass()
print("North: %s" % north)

raw = sense.get_compass_raw()
print("x: {x}, y: {y}, z: {z}".format(**raw))

# alternatives
print(sense.compass_raw)

o = sense.get_orientation() #orientation is dictionary {'roll': 360.000, 'pitch':360.000, 'yaw':360.000}
yaw = o["yaw"]
print(f'yaw: {yaw:03.1f}')

if 178 < yaw < 182:
    print("STOP")
