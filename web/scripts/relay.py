#Import all neccessary features to code.
import RPi.GPIO as GPIO
from time import sleep

#If code is stopped while the solenoid is active it stays active This
#may produce a warning if the code is restarted and it finds the GPIO
#Pin, which it defines as non-active in next line, is still active
#from previous time the code was run. This line prevents that warning
#syntax popping up which if it did would stop the code running.
GPIO.setwarnings(False)
#This means we will refer to the GPIO pins by the number directly
#after the word GPIO. A good Pin Out Resource can be found here
#https://pinout.xyz/
GPIO.setmode(GPIO.BCM)
#setup the GPIO pins as output
relay1pin = 23
relay2pin = 24
relay3pin = 25

GPIO.setup(relay1pin, GPIO.OUT)
GPIO.setup(relay2pin, GPIO.OUT)
GPIO.setup(relay3pin, GPIO.OUT)

#turn everybody off

def everybody_off():
    GPIO.output(relay1pin, 1)
    GPIO.output(relay2pin, 1)
    GPIO.output(relay3pin, 1)
    print("everybody off")

everybody_off()

try:
 
    while (True):    

        GPIO.output(relay1pin, 0)
        sleep(1)
        GPIO.output(relay1pin, 1)
        print("1 off")
        sleep(1)
        GPIO.output(relay2pin, 0)
        sleep(1)    
        GPIO.output(relay2pin, 1)
        print("2 off")    
        sleep(1)    
        GPIO.output(relay3pin, 0)
        sleep(1)
        GPIO.output(relay3pin, 1)
        print("3 off")

        sleep(1)
except KeyboardInterrupt:
    print('KeyboardInterrupt')
    everybody_off()
    
