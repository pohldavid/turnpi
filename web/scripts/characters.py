X = [255, 0, 0]  # Red
O = [0, 0, 0]  # off
W = [255,255,255]

question_mark = [
O, O, O, X, X, O, O, O,
O, O, X, O, O, X, O, O,
O, O, O, O, O, X, O, O,
O, O, O, O, X, O, O, O,
O, O, O, X, O, O, O, O,
O, O, O, X, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, X, O, O, O, O
]

arrow = [
O, O, O, X, O, O, O, O,
O, O, O, X, O, O, O, O,
O, O, O, X, O, O, O, O,
O, O, O, X, O, O, O, O,
X, O, O, X, O, O, X, O,
O, X, O, X, O, X, O, O,
O, O, X, X, X, O, O, O,
O, O, O, X, O, O, O, O
]

all_white = [
W,W,W,W,W,W,W,W, 
W,W,W,W,W,W,W,W, 
W,W,W,W,W,W,W,W, 
W,W,W,W,W,W,W,W, 
W,W,W,W,W,W,W,W, 
W,W,W,W,W,W,W,W, 
W,W,W,W,W,W,W,W,
W,W,W,W,W,W,W,W,
]
