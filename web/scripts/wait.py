import time
from datetime import datetime, timedelta



def wait():
    # Calculate the delay to the start of the next hour
    next_hour = (datetime.now() + timedelta(hours = 1)).replace(
        minute=0, second=0, microsecond=0)
    delay = (next_hour - datetime.now()).seconds
    #time.sleep(delay)
    return delay


print(f'seconds till next hour {wait()}')