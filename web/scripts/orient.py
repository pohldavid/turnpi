from time import sleep
from sense_hat import SenseHat
sense = SenseHat()

def orientation():
    
    o = sense.get_orientation() #orientation is dictionary {'roll': 360.000, 'pitch':360.000, 'yaw':360.000}
    
    pitch = o["pitch"]
    roll = o["roll"]
    yaw = o["yaw"]
    
    north = sense.get_compass()
            
    print(f'pitch: {pitch:03.1f} roll: {roll:03.1f} yaw: {yaw:03.1f}')
    print(f'North: {north}')

    


