
import sqlite3

""" CREATE TABLE """

conn = sqlite3.connect('test.db')
# Fri 11 Mar 2022 02:41:15 PM CST 50.9 F 29.89 InHg
conn.execute('''CREATE TABLE weatherdata
         (ID INT PRIMARY KEY     NOT NULL,
         DayName,
         Day,
         Month,
         Year,
         Time,
         AMPM,
         TZ,
         Temperature,
         TempUnit,
         Humidity,
         HumidityUnit,
         Pressure,             
         PressureUnit);''')
         
print("Table created successfully")
conn.close()


""" INSERT DATA INTO TABLE """


conn = sqlite3.connect('test.db')
conn.execute("INSERT INTO weatherdata (ID,Temperature) VALUES (1, 56.67)")
conn.commit()
conn.close()


""" PRINT TABLE """

conn = sqlite3.connect('test.db')
cursor = conn.execute("SELECT ID, Temperature from weatherdata")
print(cursor)
for row in cursor:
    print("ID = ", row[0])
    print("Temperature = ", row[1])

print("Records created successfully")
conn.close()
# visit pyshine.com for more detail
""" UPDATE TABLE """

conn = sqlite3.connect('test.db')
conn.execute("UPDATE weatherdata set Temperature = 20.00 where ID = 1")
conn.commit()
conn.close()

""" PRINT TABLE """

conn = sqlite3.connect('test.db')
cursor = conn.execute("SELECT ID, Temperature")
print(cursor)
for row in cursor:
    print("ID = ", row[0])
    print("Temperature = ", row[1], "\n")

print("Records created successfully")
conn.close()
