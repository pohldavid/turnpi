#! /usr/bin/python3

import argparse
parser = argparse.ArgumentParser()

parser.add_argument("angle", type= int, help="rotation angle in degrees. Negative = CCW")

args = parser.parse_args()


def duration(angle):
    rpm = 0.5
    duration = angle/(rpm/60*360)
    print(f"{duration} seconds")
    return duration
    


    


r = args.angle

duration(r) 

if r < 0:
    dir = 'Counterclockwise <---'
else:
    dir = 'Clockwise --->'
    
rotate = str(r).strip('-')

print(f'rotate {rotate} degrees {dir} in approximately {duration(r)} seconds')