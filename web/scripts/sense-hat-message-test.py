#!/usr/bin/python3

"""
if button is momentarily pressed reboot 
if button is held 3 seconds shutdown

make executable and
add to crontab -e
@reboot /path/to_this_script/shutdown.py
"""
import os
import time
from signal import pause
from sense_hat import SenseHat
from gpiozero import Button

sense = SenseHat()
sense.set_rotation(90)
sense.show_message('REBOOT', text_colour=[0,255,0], scroll_speed = 0.07)

 