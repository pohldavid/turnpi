import sqlite3

#set up connection
connection = sqlite3.connect("aquarium.db")

#create table
cursor = connection.cursor()
cursor.execute("CREATE TABLE fish (name TEXT, species TEXT, tank_number INTEGER)")

#insert data
cursor.execute("INSERT INTO fish VALUES ('Sammy', 'shark', 1)")
cursor.execute("INSERT INTO fish VALUES ('Jamie', 'cuttlefish', 7)")

#read data
rows = cursor.execute("SELECT name, species, tank_number FROM fish").fetchall()
print(rows)

