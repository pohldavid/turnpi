import time

n = 0

while True:
    if n == 359:
        n = 0
    else:
        n += 1

    if n == 0:
        direction = f'Bearing: North'

    elif 0 < n < 90:
        direction = f'Bearing: N {n:02} Degrees E'

    elif n == 90:
        direction = f'Bearing: East'

    elif 90 < n < 180:
        d = 180 - n
        direction = f'Bearing: S {d:02} Degrees E'

    elif n == 180:
        direction = f'Bearing: South'

    elif 180 < n < 270:
        d = n - 180
        direction = f'Bearing: S {d:02} Degrees W'

    elif n == 270:
        direction = f'Bearing: West'

    elif 270 < n < 360:
        d = 360 - n
        direction = f'Bearing: N {d:02} Degrees W'
        
    heading = f'Heading: {n:02} degrees'

    with open("data", 'w') as outfile:
        outfile.write(str('<p>' + heading + '</p>'  '<p>' + direction + '</p>'))
        print(direction)
    outfile.close()
    time.sleep(1)
