import datetime


def get_date_and_time():
    now = datetime.datetime.now()

    # see strftime format codes here https://docs.python.org/3.2/library/datetime.html?highlight=date#datetime

    format = "%Y-%m-%d %I:%M:%S %p%Z"

    #print('current date and time',now.strftime(format))

    return now.strftime(format)


if __name__ == '__main__':

    print(get_date_and_time())
