
from flask import Flask, render_template, redirect, url_for
from forms import DirectionForm


from fetch_data import fetch


app = Flask(__name__)
app.config['SECRET_KEY'] = 'your secret key'



@app.route('/', methods=('GET', 'POST'))
def index():
#    fetch()
    form = DirectionForm()
    return render_template('index.html', form=form)



if __name__ == "__main__":
    app.run(debug = True, host = "0.0.0.0", port = 8000)
